package main

import (
	"log"

	"gitlab.com/ffSaschaGff/tarot/internal/scraper"
)

func main() {
	log.SetFlags(log.Lshortfile)
	scrapingService := scraper.NewScraper("https://astrohelper.ru", "/gadaniya/taro/znachenie")
	scrapingService.Scrap()
}
