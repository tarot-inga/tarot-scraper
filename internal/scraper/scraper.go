package scraper

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gocolly/colly"
)

type scraper struct {
	root  string
	url   string
	cards []card
}

type Scraper interface {
	Scrap()
}

type card struct {
	Name     string   `json:"name"`
	imgUrl   string   `json:"-"`
	UpDesc   []string `json:"up_desc"`
	DownDesc []string `json:"down_desc"`
}

func NewScraper(
	root string,
	url string,
) Scraper {
	return &scraper{
		url:  url,
		root: root,
	}
}

func (s *scraper) Scrap() {
	s.cards = nil
	rootScraper := colly.NewCollector()
	rootScraper.OnError(failOnErr)
	rootScraper.OnHTML("a.zodi-space", s.rootCardHandler)
	rootScraper.Visit(s.root + s.url)

	err := s.save()
	if err != nil {
		log.Fatalln(err)
	}
}

func (s *scraper) save() error {
	for _, onewCard := range s.cards {
		err := saveImg(onewCard.imgUrl, onewCard.Name)
		if err != nil {
			return err
		}
	}

	json, err := json.Marshal(&s.cards)
	if err != nil {
		return err
	}
	file, err := os.Create("./resources/cards.json")
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = io.WriteString(file, string(json))
	if err != nil {
		return err
	}

	return nil
}

func (s *scraper) rootCardHandler(rootElement *colly.HTMLElement) {
	newCard := card{
		Name: rootElement.Attr("title"),
	}

	currentCardScrapper := colly.NewCollector()
	currentCardScrapper.OnError(failOnErr)
	currentCardScrapper.OnHTML("img", func(imgElement *colly.HTMLElement) {
		if strings.HasSuffix(imgElement.Attr("alt"), ": что значит аркан") {
			newCard.imgUrl = s.root + removeGoDown(imgElement.Attr("src"))
		}
	})
	currentCardScrapper.OnHTML("table.table.table-striped tbody", func(tableElement *colly.HTMLElement) {
		tableElement.ForEach("tr", func(i int, tableRowElement *colly.HTMLElement) {
			switch tableRowElement.ChildText("td:first-child") {
			case "Прямое положение:":
				newCard.UpDesc = strings.Split(tableRowElement.ChildText("td:last-child"), ", ")
			case "Перевернутое положение:":
				newCard.DownDesc = strings.Split(tableRowElement.ChildText("td:last-child"), ", ")
			}
		})
	})
	currentCardScrapper.Visit(s.root + s.url + "/" + rootElement.Attr("href"))

	for index := range newCard.UpDesc {
		newCard.UpDesc[index] = strings.ToLower(newCard.UpDesc[index])
	}
	for index := range newCard.DownDesc {
		newCard.DownDesc[index] = strings.ToLower(newCard.DownDesc[index])
	}
	s.cards = append(s.cards, newCard)
}

func failOnErr(r *colly.Response, err error) {
	log.Fatal(err)
}

func saveImg(url, name string) error {
	img, err := os.Create("./resources/imgs/" + name + ".jpg")
	if err != nil {
		return err
	}
	defer img.Close()

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	_, err = io.Copy(img, resp.Body)
	return err
}

func removeGoDown(url string) string {
	return strings.ReplaceAll(url, "/..", "")
}
